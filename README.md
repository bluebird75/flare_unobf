flare_unobf
===========
Par Philippe Fremy 

Ce petit sript permet de transformer un fichier flare obfusqué en un fichier un peu plus lisible, en particulier au niveau des identifiants. Il a été développer pour gérer les fichiers flash obfusqué des jeux de la Motion Twin. 


Exemple
-------

*Avant:*

    v1. ]-YE = function () {
      this.3L]P6 = !this.3L]P6;
       S(Q.,O.1z-._pause.smc.gotoAndStop(this.3L]P6 ? 2 : 1);
       S(Q.,O.1z-._pause.gotoAndStop(1);
      if (this.(j-- () {
        if (!this.3L]P6) {
          this.*g+VS();
        } else {
          this.8X2q2.38,1.stop();
        }
      }
      var v2 = this.[ay9.iterator();
      while (v2.[sK+)()) {
        var v3 = v2.2Q{6();
        if (!v3.1hC[4) {
          continue;
        }
        v3.38,1.stop();
        v3.1hC[4 = false;
      }
    };



*Après:*

    v1.func_249 = function () {
      this.attr_77 = !this.attr_77;
       func_58.attr_47.attr_61._pause.smc.gotoAndStop(this.attr_77 ? 2 : 1);
       func_58.attr_47.attr_61._pause.gotoAndStop(1);
      if (this.attr_178) {
        if (!this.attr_77) {
          this.func_246();
        } else {
          this.attr_182.attr_53.stop();
        }
      }
      var v2 = this.attr_184.iterator();
      while (v2.func_125()) {
        var v3 = v2.func_126();
        if (!v3.func_234) {
          continue;
        }
        v3.attr_53.stop();
        v3.func_234 = false;
      }
    };




Limitations
-----------

Vous aller surement rencontrer les limitations suivantes:
* comme un nom obfusqué peut correspondre à plusieurs noms rééls, il y aura des nommages un peu incohérents
* certaines constructions sont difficiles à désobfusquer, il va rester des identifiants obfusqués


Exemple:

    this.attr_247.func_145(this.+();

L'identifiant `+(` rest obfusqué car il est trop proche de la syntaxe du langage pour être détecté.

Pour faire mieux, il faudrait analyser en détail la syntaxe du fichier ActionScript, ce qui est beaucoup plus long et délicat à développer. En tout cas, la version actuelle fait déjà du bien au fichier.


Mode d'emploi
-------------

Vous aller partir d'un fichier deccomiplé par flare, par exemple `mon_jeu.flr`. La première étape est de repérer les noms possibles dans le fichier et de générer un dictionnaire au format json:

    python unobf.py find_names mon_jeu.flr unobf.json


Le fichier `unobf.json` a été créé. Veuillez bien l'inspecter pour voir si des substitions en trop ne s'y trouvent pas. Par exemple, il arrive que des identifiants valides soit substitutés:

    "_score": "attr_153",
    "toString": "func_127",


Dans ce cas, il faut les déplacer dans la section `"dont_touch"`. Cela indique au script qu'il faut laisser ces identifiants tranquilles. Sans cette indication, le script n'a aucun moyen de faire la différence entre un identifiant lisible, genre `length` et un identifiant obfusqué mais vaguement lisible, genre `aTF56kh_`  . Il faut l'aider un peu pour pas qu'il fasse de bétise.

Une fois que votre dictionnaire de substitution est correct, vous pouvez lancer la substitution:

    python unobf.py apply_unobf mon_jeu.flr unobf.json

Et vous aurez un beau fichier `mon_jeu.flr` désobfusqué. Vous pouvez réutiliser les même dictionnaire sur plusieurs fichiers, ou lancer la désobfuscation plusieurs fois.


Le dernier mode, c'est quan vous êtes en train d'éditer manuellement le dictionnaire pour améliorer l'analyse du code.

    python unobf.py watch_apply_unobf mon_jeu.flr unobf.json

Mettons que vous découvrez que `_6,33b` est en fait la fonction `min`, vous allez le rajouter dans le dictionnaire. Le script va détecter une modification du dictionnaire et lancer aussitôt une substitution.

Voilà, n'hésitez pas à faire des retour, soit ici, soit sur le discord _eternaltwin_ .




