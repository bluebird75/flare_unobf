# Released under the WTF Public License (see http://www.wtfpl.net/)

from typing import List, Dict, Union, re as re_type
import time, sys, json, re

def parse_list_of_strings(enum_content):
	# "['4GFR6(', '{uc*Y', '1sLyX', ']jt5((', '6aXS3(', '5aXS3('];"
	#
	enum_content = enum_content.strip()
	if enum_content[-1] == ';':
		enum_content = enum_content[:-1]
	assert enum_content[0] == '['
	assert enum_content[-1] == ']'
	return eval(enum_content)


def split_object_attr(s: str) -> List[str]:
	#   func_55.,O.attr_80.attr_122[v9.attr_87][v13]
	# 		--> "func_55",  ",0", "attr_80", "attr_122"
	#   v2[object_35.func_443.attr_479.cca(v5)] = v5;
	#		--> "v2"
	#   this.attr_122[arg0.+(][arg0.*(]
	#		--> "this", "attr_122"
	#   v4[v4.length + v6 - 1]
	#		--> "v4"
	ret = []
	while len(s):
		# trim array part
		while '[' in s and ']' in s:
			if s[-1] == ']':
				# array access, find the matching [
				open_brace = s.rfind('[', 0, len(s)-1)
				if open_brace == -1:
					# can not split this
					return []

				s = s[:open_brace]
			else:
				break

		dot_pos = s.rfind('.')
		if dot_pos == -1:
			ret.append(s)
			break
		else:
			tail = s[dot_pos+1:]
			s = s[:dot_pos]
			ret.append(tail)

	ret.reverse()
	return ret

def self_test():
	assert (split_object_attr("func_55.,O.attr_80.attr_122[v9.attr_87][v13]")
			== ["func_55",  ",O", "attr_80", "attr_122"])
	assert (split_object_attr("v2[object_35.func_443.attr_479.cca(v5)]")
			== ["v2"])
	assert (split_object_attr("this.attr_122[arg0.+(][arg0.*(]")
			== ["this", "attr_122"])
	assert (split_object_attr("v4[v4.length + v6 - 1]")
			== ["v4"])
	assert (split_object_attr("aaa.b[bb.ccc")
			== ["aaa", "b[bb", "ccc"])
	assert (split_object_attr("aaa.b]bb.ccc")
			== ["aaa", "b]bb", "ccc"])
	assert (split_object_attr("aaa.b[bb[33].ccc")
			== ["aaa", "b[bb", "ccc"])
	assert (split_object_attr("aaa.b]bb[33].ccc")
			== ["aaa", "b]bb", "ccc"])
	assert (split_object_attr("aaa.x[b]bb].ccc")
			== ["aaa", "x", "ccc"])
	assert (split_object_attr("aaa.x[b[bb].ccc")
			== ["aaa", "x[b", "ccc"])
	assert (parse_list_of_strings("['4GFR6(', '{uc*Y', '1sLyX', ']jt5((', '6aXS3(', '5aXS3('];")
			== ['4GFR6(', '{uc*Y', '1sLyX', ']jt5((', '6aXS3(', '5aXS3('])

# [GNh1 = new Object();
# -> object
# [GNh1.__constructs__ = ['[U]ps', '6n}c', '7S1X', '{G(R', '1rfr*(', '8QsRL', '-D}L[', '[nKd1', '6O+S'];
# -> enum values


def strip_array_access_if_any(s: str) -> str:
	# toto[1] -> toto
	# toto[2][3] -> toto
	# "dkf[dkf[2] -> dkf[dkf
	s = s.strip()
	while len(s) and s[-1] == ']' and (s.count('[') == s.count(']')):
		# we may have an array access
		open_brace = s.rfind('[', 0, len(s)-1)
		if open_brace == -1:
			# count not find any other [], that's strange but ok
			return s
		s = s[:open_brace]

	return s

def read_unobf_json(fname_json):
	'''Return the dictionnary used to substitute'''
	with open(fname_json) as f:
		unobf = f.read()

	unobf_d = json.loads(unobf)
	return unobf_d


def create_unobf_json(fname_json):
	'''Return the dictionnary used to substitute'''
	print('Starting from an empty substitution dictionnary')
	unobf_d = {
		'array': {},
		'attr': {},
		'enum': {},
		'function': {},
		'object': {},
		'dont_touch': dont_touch
	}
	write_unobf_json(fname_json, unobf_d)
	return unobf_d



def write_unobf_json(fname_json, d):
	with open(fname_json, 'w') as f:
		json.dump(d, f, indent=4)


def is_orig_present_in_json(orig, unobf_d):
	for category in unobf_d:
		if orig in unobf_d[category]:
			return True

	return False


def	is_dest_present_in_json(dest, unobf_d):
	for category in unobf_d:
		if category == 'dont_touch':
			if dest in unobf_d[category]:
				return True
		else:
			if dest in unobf_d[category].values():
				return True

	return False

def is_orig_a_bad_idea(orig, unobf_d):
	'''Return whether this new orig should not be added'''
	def check(orig): return (
			is_orig_present_in_json(orig, unobf_d)
			or is_dest_present_in_json(orig, unobf_d)
			or len(orig) == 0
			or len(orig) == 1
			or (orig[0] == '[' and orig[-1] == ']')
			or (orig == '()')
			or (orig.isdigit())
			or (orig[0] == '_' and orig[1:].isdigit())
			or (orig[0] == 'v' and orig[1:].isdigit())
			or (orig[:3] == 'arg')
			or orig.startswith('var ')
	)
	return check(orig) or check(orig.strip())
# we already have it


def extract_array_info(l, i, unobf_d, possible_object):
	# XXX = new Array -> XXX is an array
	#	var v1 = new Array() 
	#		-> this is scoped, can not use it globally
	#   return new Array()
	#		-> not really useful
	#   xxx.yyy.zzz = new Array()
	#		-> attribute zzz is an array

	marker = '= new Array()'
	if marker not in l:
		return

	array_end = l.find(marker)
	array_start = l.rfind('.', 0, array_end)
	if array_start == -1:
		# local variable array, can not substitute globally
		return ''
	else:
		# scoped array, can extract namespace
		possible_object.extend( [(i, name)
								 for name in split_object_attr(l[:array_start].strip())] )

	orig = l[array_start+1:array_end].rstrip()

	orig = strip_array_access_if_any(orig)

	if is_orig_a_bad_idea(orig, unobf_d):
		return

	idx = 1
	dest = 'array_%02d' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'array_%02d' % idx

	print('New array at line %d: "%s" -> "%s"' % (i, orig, dest))
	unobf_d['array'][orig] = dest


def extract_object_info(l, i, unobf_d, possible_object):
	# XXX.__name__ = ['XXX'] -> XXX is an object
	# XXX.__ename__ = ['XXX'] -> XXX is a object
	# yyy.XXX.__name__ = ['yyy', 'XXX'] -> yyy.XXX is a object
	#								    -> yyy is an object

	marker = ".__name__ = ['"
	if marker not in l:
		marker = ".__ename__ = ['"
		if marker not in l:
			return

	object_1_end = l.find(marker)
	object_2_start = object_1_end + len(marker)
	object_2_end = l.find("'", object_2_start)
	while l[object_2_end+1] != ']' and object_2_end < len(l):
		possible_object.append((i, l[object_2_start:object_2_end]))
		object_2_start = l.find("'", object_2_end+1)+1
		object_2_end = l.find("'", object_2_start)

	if object_2_end == -1 or object_2_end >= len(l):
		return

	object_2_name = l[object_2_start:object_2_end]
	object_1_name = l[object_1_end-len(object_2_name):object_1_end]

	orig = object_1_name
	if is_orig_a_bad_idea(orig, unobf_d):
		# we already have it
		return

	assert object_1_name == object_2_name

	idx = 1
	dest = 'object_%02d' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'object_%02d' % idx

	print('New object at line %d: "%s" -> "%s"' % (i, orig, dest))
	unobf_d['object'][orig] = dest

	return


def extract_object_info2(l, i, unobf_d, possible_object):
	# XXX = function () {} -> XXX is a namespace or a object
	marker = " = function () {};"
	if marker not in l:
		return

	object_1_end = l.find(marker)

	# check if this is a nested object
	object_1_start = l.rfind('.', 0, object_1_end)
	if object_1_start != -1:
		object_name = l[object_1_start+1:object_1_end]
		possible_object.extend( [(i, name)
								 for name in split_object_attr(l[:object_1_start].strip())] )
	else:
		# this is a base object, without nesting
		object_name = l[:object_1_end].strip()

	orig = object_name
	if is_orig_a_bad_idea(orig, unobf_d):
		# we already have it
		return

	idx = 1
	dest = 'object_%02d' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'object_%02d' % idx

	print('New object at line %d: "%s" -> "%s"' % (i, orig, dest))
	unobf_d['object'][orig] = dest

	return

def extract_enum_info(l, i, unobf_d, possible_object):
	# )MSyH.__constructs__ = ['4GFR6(', '{uc*Y', '1sLyX', ']jt5((', '6aXS3(', '5aXS3('];
	# -> the content of the list are enum members with value of the index
	marker = ".__constructs__ = ["
	if marker not in l:
		return

	# assumption: enumeration is on one line
	enum_content = l[l.find(marker) + len(marker)-1:]
	enum_list = parse_list_of_strings(enum_content)

	for orig in enum_list:
		if is_orig_a_bad_idea(orig, unobf_d):
			# we already have it or we do not want it
			return

	# find the enum slot available
	idx = 1
	dest = 'enum_%02d_00' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'enum_%02d_00' % idx

	for j, orig in enumerate(enum_list):
		dest = 'enum_%02d_%02d' % (idx, j)
		print('New enum value at line %d: "%s" -> "%s"' % (j, orig, dest))
		unobf_d['enum'][orig] = dest

	return



def extract_function_info(l, i, unobf_d, possible_object):
	# XXX = function ( -> XXX is a function
	marker = " = function ("
	if marker not in l:
		return []

	func_end = l.find(marker)
	if l.find('}', func_end + len(marker)) != -1:
		# there is a closing brace, this is not a function but a object
		return

	# check if this is a method of a object
	func_start = l.rfind('.', 0, func_end)
	if func_start != -1:
		func_name = l[func_start+1:func_end]
		possible_object.extend( [(i, name)
								 for name in split_object_attr(l[:func_start].strip())] )
	else:
		# this is direct function
		func_name = l[:func_end].strip()

	orig = func_name
	if is_orig_a_bad_idea(orig, unobf_d):
		# we already have it
		return

	idx = 1
	dest = 'func_%02d' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'func_%02d' % idx

	print('New function at line %d: "%s" -> "%s"' % (i, orig, dest))
	unobf_d['function'][orig] = dest

	return


def extract_number_attr(l, i, unobf_d, possible_object):
	# xxx.yyy = 33;  -> yyy is an attribute, xxx is a object
	marker = re.compile(r" = \d+(\.\d+)?;")
	return extract_attr(l, i, unobf_d, possible_object, marker)


def extract_attr(l: str, i: int, unobf_d: Dict, possible_object: List, marker: Union[str, re_type]) -> None:
	if type(marker) == str:
		re_marker = re.compile(re.escape(marker))
	else:
		re_marker = marker

	mo = re_marker.search(l)
	if not mo:
		return

	# "= number" must be the last part of the line
	if mo.end() != len(l.rstrip()):
		return

	attr_end = mo.start()

	# check if this is an actual attribute
	names = split_object_attr(l[:attr_end].strip())
	if len(names) < 2:
		# direct assignment or no attribute
		return

	orig = names[-1]
	if is_orig_a_bad_idea(orig, unobf_d):
		# we already have it or we don't want it
		return

	possible_object.extend([(i, name) for name in names[:-1]])

	idx = 1
	dest = 'attr_%02d' % idx
	while is_dest_present_in_json(dest, unobf_d):
		idx += 1
		dest = 'attr_%02d' % idx

	print('New attribute at line %d: "%s" -> "%s"' % (i, orig, dest))
	unobf_d['attr'][orig] = dest

	return


def extract_null_attr(l, i, unobf_d, possible_object):
	# xxx.yyy = null;  -> yyy is an attribute, xxx is a object
	marker = " = null;"
	return extract_attr(l, i, unobf_d, possible_object, marker)


def extract_all_attr(l, i, unobf_d, possible_object):
	# xxx.zzz = ...  -> zzz is an attribute, xxx is a object
	# xxx.yyy.zzz = ...  -> zzz is an attribute, xxx and yyy is a object
	#
	# beware of xxx[33].yyyy[43] = ...

	marker = ' = '
	return extract_attr(l, i, unobf_d, possible_object, marker)


def	extract_possible_object(unobf_d, possible_object):
	# names to convert into a object
	for i, orig in possible_object:
		orig = strip_array_access_if_any(orig)
		if is_orig_a_bad_idea(orig, unobf_d):
			# we already have it
			continue

		idx = 1
		dest = 'object_%02d' % idx
		while is_dest_present_in_json(dest, unobf_d):
			idx += 1
			dest = 'object_%02d' % idx

		print('New object at line %d: "%s" -> "%s"' % (i, orig, dest))
		unobf_d['object'][orig] = dest


def find_names(fname_flr, fname_json):
	print('Finding names for substitution')
	# unobf hints:
	# Done:
	# XXX = new Array -> XXX is an array
	# XXX.__name__ = ['XXX'] -> XXX is a object
	# XXX.__ename__ = ['XXX'] -> XXX is a object
	# XXX = function () {} -> XXX is a object
	# XXX = function () { -> XXX is a function
	# xxx.yyy = null;  -> yyy is an attribute, xxx is a object
	# xxx.yyy = 33.0;  -> yyy is an attribute, xxx is a object
	# Rejected:
	# xxx.yyy = ... -> yyy is an attribute where "..." is not "function"
	# xxx.zzz.yyy = ... -> zzz is an attribute if not already a object

	try:
		unobf_d = read_unobf_json(fname_json)
	except FileNotFoundError:
		# we must create the file
		unobf_d = create_unobf_json(fname_json)

	with open(fname_flr) as f:
		lines = f.readlines()

	possible_object = []

	for i, l in enumerate(list(lines)):
		l = l[:-1]
		extract_array_info(l, i, unobf_d, possible_object)
		extract_object_info(l, i, unobf_d, possible_object)
		extract_object_info2(l, i, unobf_d, possible_object)
		extract_enum_info(l, i, unobf_d, possible_object)
		extract_function_info(l, i, unobf_d, possible_object)
		extract_null_attr(l, i, unobf_d, possible_object)
		extract_number_attr(l, i, unobf_d, possible_object)
		extract_all_attr(l, i, unobf_d, possible_object)

	# process candidate namespace at the end, to let other substitutions
	# apply first.
	extract_possible_object(unobf_d, possible_object)
	write_unobf_json(fname_json, unobf_d)


def apply_unobf(fname_flr, fname_json):
	print('===> appplying substitutions to unobfuscation')
	with open(fname_flr) as f:
		content = f.read()

	nb_tot_subst = 0
	unobf_d = read_unobf_json(fname_json)
	subst = []

	for category in unobf_d:
		if category == 'dont_touch':
			continue
		for orig, dest in unobf_d[category].items():
			subst.append((orig, dest))

	subst.sort(key=lambda v:len(v[0]), reverse=True)
	for orig, dest in subst:
		if orig in unobf_d['dont_touch']:
			continue
		print('Substituing "%s" (length %d) with %s: %d occurences' % (orig, len(orig), dest, content.count(orig)))
		nb_tot_subst += content.count(orig)
		if len(orig) > 2:
			# orig must be surrounded by non alphanumeric
			content = re.sub('(\\W?)(%s)(\\W)' % re.escape(orig), '\\1%s\\3' % re.escape(dest), content)
		else:
			# for len(orig) == 2, orig must be surrounded by '.' or ' '
			content = re.sub('([ \\."\'])(%s)([ \\."\'])' % re.escape(orig), '\\1%s\\3' % re.escape(dest), content)

	print('Total %d substitutions' % nb_tot_subst)

	with open(fname_flr, 'w') as f:
		f.write(content)


def watch_apply_unobf(fname_flr, fname_json):
	with open(fname_json) as f:
		last_json = f.read()

	while True:
		with open(fname_json) as f:
			unobf_json = f.read()

		if unobf_json != last_json:
			apply_unobf(fname_flr, fname_json)
			last_json = unobf_json

		time.sleep(5)

USAGE='''
unobf.py find_names file_flr file_json
	Fills file_json with all names infer from file_flr.
	Please double-check the result for mistakes.
	
unobf.py apply_unobf file_flr file_json
	Uses file_json to replace names inside file_flr.
	Better make a bakcup before performing this.
	
unobf.py watch_apply_unobf file_flr file_json
	Watch mode for apply_unobf: each time the file_json is modified,
	run apply_unobf.
'''

def main():
	if len(sys.argv) != 4:
		print('You must supply two arguments: file_flr and file_json')
		print()
		print(USAGE)
		sys.exit(-1)

	if 'apply_unobf' in sys.argv:
		apply_unobf(sys.argv[2], sys.argv[3])
		sys.exit(0)

	if 'watch_apply_unobf' in sys.argv:
		watch_apply_unobf(sys.argv[2], sys.argv[3])
		sys.exit(0)

	if 'find_names' in sys.argv:
		find_names(sys.argv[2], sys.argv[3])
		sys.exit(0)

	print('ERROR, bad command-line')
	sys.exit(-1)


dont_touch = [
        "this",
        "prototype",
        " new_func_382_element_from_proba_array",
        " new_random_element_from_proba_array",
        "0O",
        "__class__",
        "__clear_trace",
        "__closure",
        "__enum__",
        "__exc",
        "__init",
        "__instanceof",
        "__set_trace_color",
        "__string_rec",
        "__trace",
        "__trace_lines",
        "_alpha",
        "_Art",
        "_ArtefactId",
        "_Block",
        "_bno",
        "_byes",
        "_Col",
        "_Catz",
        "_Choco",
        "_Dalton",
        "_Detartrage",
        "_Empty",
        "_Gift",
        "_GodFather",
        "_Joker",
        "_MentorHand",
        "_Neutral",
        "_NowelBall",
        "_Pa",
        "_Patchinko",
        "_Pistonide",
        "_PolarBomb",
        "_RazKroll",
        "_Skater",
        "_SnowBall",
        "_Stamp",
        "_Tejerkatum",
        "_Teleport",
        "_Unknown",
        "_Wombat",
        "_CountBlock",
        "_Delorean",
        "_Destroyer",
        "_DigReward",
        "_Dollyxir",
        "_Dynamit",
        "_Elt",
        "_Elts",
        "_Fx",
        "_global",
        "_Grenade",
        "_Jeseleet",
        "_music",
        "_pause",
        "_PearGrain",
        "_ProductData",
        "_Protoplop",
        "_Pumpkin",
        "_QuestObj",
        "_quit",
        "_Recipe",
        "_root",
        "_rotation",
        "_Sct",
        "_selected",
        "_sfx",
        "_Slide",
        "_Special",
        "_Surprise",
        "_vol",
        "_volMc",
        "_x",
        "_xscale",
        "_y",
        "_yscale",
        "apply_render",
        "apply_render_bis",
        "Array",
        "backgroundColor",
        "charCodeAt",
        "Float",
        "fromString",
        "fromTime",
        "func_func_343_chute",
        "func_func_343_chute_choc",
        "insert",
        "isFinite",
        "isNaN",
        "iterator",
        "length",
        "Math",
        "new_func_382_element_from_proba_array_attr",
        "now",
        "onData",
        "onHTTPStatus",
        "onLoad",
        "onLoadError",
        "onLoadInit",
        "onLoadProgress",
        "onMouseMove",
        "onPress",
        "onRelease",
        "onReleaseOutside",
        "onRollOut",
        "onRollOver",
        "onSoundComplete",
        "playprototype",
        "push",
        "random",
        "remove",
        "render",
        "sprite",
        "stop",
        "String",
        "textColor",
        "blendMode",
        "__super__",
        "__ename__",
        "__constructs__",
        "text",
        "_visible",
        "_group_mask",
        "_force_group_mask",
        "_Alchimoth",
        "__name__",
        "onEnterFrame",
        "__interfaces__",
        "onLoadComplete",
        "obj",
        "_next",
        "_sfloat",
        "_shake",
        "__proto__",
        "font",
        "selectable",
        "copy",
        "cca",
        "flash",
        "align",
        "_MovieClip",
        "matrix",
        "blurX",
        "blurY",
        "strength",
        "color",
        "inner",
        " eP3k",
        "background",
        "multiline",
        "wordWrap",
        "NaN",
        "NEGATIVE_INFINITY",
        "POSITIVE_INFINITY",
        "Int",
        "Bool",
        "Color",
        "Key",
        "LoadVars",
        "MovieClip",
        "MovieClipLoader",
        "Sound",
        "Stage",
        "TextField",
        "TextFormat",
        "TextSnapshot",
        "Security",
        "StyleSheet",
        "_score",
        "_field",
        "ra",
        "ga",
        "ba",
        "aa",
        "rb",
        "gb",
        "bb",
        "ab",
        "toString"
]

if __name__ == '__main__':
	self_test()
	main()


